package cz.uhk.ppro.inzerat.controller;

import cz.uhk.ppro.inzerat.model.Inzerat;
import cz.uhk.ppro.inzerat.service.PametoveUlozisteInzeratu;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class InzeratController {

    int number = 0;
    PametoveUlozisteInzeratu uloziste = new PametoveUlozisteInzeratu();

    @GetMapping(value = {"/", "/index"})
    public String domu(Model model) {

        model.addAttribute("inzeraty", uloziste.getInzeraty());
        return "index";
    }

    @GetMapping(value = {"/create"})
    public String pridej(Model model) {

        model.addAttribute("inzerat", new Inzerat());

        return "create";
    }

    @PostMapping(value = {"/create"})
    public String pridej(@ModelAttribute("inzerat") Inzerat inzerat) {

        inzerat.setId(number++);
        uloziste.pridej(inzerat);

        return "redirect:index";
    }

    @GetMapping(value = {"/update"})
    public String uprav(@RequestParam("id") int id, Model model) {

        Inzerat inzerat = uloziste.getById(id);
        model.addAttribute("inzerat", inzerat);
        model.addAttribute("id", id);

        return "edit";
    }

    @PostMapping(value = {"/update"})
    public String uprav(@ModelAttribute("inzerat") Inzerat inzerat, @RequestParam("id") int id) {

        uloziste.getById(id).setText(inzerat.getText());
        uloziste.getById(id).setCena(inzerat.getCena());
        uloziste.getById(id).setKategorie(inzerat.getKategorie());

        return "redirect:../index";
    }

    @GetMapping(value = {"/delete"})
    public String odstran(@RequestParam("id") int id) {

        uloziste.odstran(id);

        return "redirect:../index";
    }
}