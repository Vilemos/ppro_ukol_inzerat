package cz.uhk.ppro.inzerat.service;

import java.util.List;

import cz.uhk.ppro.inzerat.model.Inzerat;

public interface UlozisteInzeratu {
    public List<Inzerat> getInzeraty();

    public void pridej(Inzerat i);

    public void odstran(Inzerat i);

    public void odstran(int id);

    public void uprav(Inzerat i);

    public Inzerat getById(int id);

    public List<Inzerat> getInzeratyByKategorie(String kategorie);
}
